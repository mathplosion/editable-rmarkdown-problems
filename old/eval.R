library(shiny)
library(knitr)
library(rlang)
library(globals)

reactiveConsole(TRUE)

get_lhs_rhs<-function(expr) {
    # should check this is an assignment
	lhs<-deparse(expr[[2]])
	rhs<-deparse(expr[[3]])
    parts<-c()
    parts$lhs<-lhs
    parts$rhs<-rhs
    return (parts) 
}

params<-reactiveValues(x=3,num=5)

lines<-c("pp<-params[['x']]+params[['num']]",
         "q<-params[['x']]",
         "mu<-mean(c(pp,q))")

#pp<-reactive({input$x+input$num})
#q<-reactive({input$x})
#mu<-reactive({mean(c(pp(),q())}))

print("original lines:")
for(i in 1:length(lines))
    print(lines[i])

my_reactives<-lapply(lines, function(line) {
    sides<-get_lhs_rhs(parse_expr(line))
    lhs<-sides$lhs
    rhs<-sides$rhs
    parsed<-parse_expr(rhs)
    vars<-intersect(findGlobals(parsed), ls(env=.GlobalEnv))
    vars<-vars[! vars %in% c("params")]
    cat(file=stderr(), "setup reactive: ", lhs, "\n")  
    assign(lhs, 
           reactive({
                cat(file=stderr(), "reactive: ", lhs, "\n")  
                args<-list()
                for(var in vars)
                    args[[var]] <- do.call(var, list())
                eval(parsed, env=do.call(rlang::env, args))
           }), 
           envir=.GlobalEnv)
})

observe({
	cat("observer 1", "\n\n")
    cat("x:", params$x, " num:", params$num, "\n")
    cat("pp:", pp(), " q:", q(), "mu: ", mu(), "\n")
})

observe({
	cat("observer 2", "\n\n")
    cat(knitr::knit_expand(text="hi there {{sqrt(pp)}}", pp=pp()), "\n") 
})

params$num<-6


reactiveConsole(FALSE)
