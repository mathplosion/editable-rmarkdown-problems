FROM rocker/shiny

RUN mkdir -p /editable-rmarkdown-problems
COPY . /editable-rmarkdown-problems

WORKDIR /editable-rmarkdown-problems

RUN R -e "install.packages('parsermd', dependencies=TRUE, repos='http://cran.us.r-project.org');"
RUN R -e "install.packages('shinyjs', dependencies=TRUE, repos='http://cran.us.r-project.org');"

EXPOSE 8080

CMD ["Rscript","run_app.R"]
